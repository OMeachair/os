#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>

char *allocate_memory(){

	char *array = malloc(USHRT_MAX);

	for(int i = 0; i < USHRT_MAX; ++i){
		array[i] = 33;
	}

	return array;
}

int check_memory(char *array, char *array_name){
	if(array == NULL){
	   printf("Malloc could not allocate memory for %s\n", array_name);
	   return 1;
	}
}

void free_memory(char *array){

	free(array);
}

int generate_random(int min, int max, int type){

	srand(time(0));
	int random = rand() % (max - min) + min;

	if(type == 1){
		while(random % 256 != 0){
			random = rand() % (max - min) + min;
		}
		random += 1;
	}

	return random;
}
