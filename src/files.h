#ifndef FILESH
#define FILESH

   FILE *open_file(char[]);
   int check_file(FILE *, char[]);
   void close_file(FILE *);

#endif
      
