
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>

#define PAGESIZE 256
#define CHARMAX  127
#define CHARMIN  34

char *bytes_for_process(char *array, int num_of_bytes, int index){

	char random_char;

	for(int i = 0; i < num_of_bytes; ++i){

		random_char = rand() % (CHARMAX - CHARMIN) + CHARMIN;
		array[index] = random_char;
		++index;
	}

	return array;
}

void write_pt_file(char *array, FILE *page_table_txt, int num_of_bytes, int index){

	int frame = num_of_bytes / PAGESIZE;

	fprintf(page_table_txt, "Page | # | PTE\n");

	for(int i = 0; i < PAGESIZE; ++i){
		fprintf(page_table_txt,"0x%X  | %d |",i,i);

		for(int j = 0; j < PAGESIZE; ++j){
			if(array[index++] > 32 && array[index++] < 128){
				fprintf(page_table_txt,"%c ",array[index++]);
			}
			else{
				fprintf(page_table_txt,"%c ",33);
			}
		}
		fprintf(page_table_txt,"\n");
	}
}

void write_pm_file(char *array, FILE *physical_mem_txt){

	int frame = 0;
	int in_use = 0;

	fprintf(physical_mem_txt,"Address | Frame | Content | In Use\n");

	for(int i = 0; i < USHRT_MAX; ++i){
		if(array[i] != 33){
			in_use = 1;
		}

		fprintf(physical_mem_txt,"0x%X     | %d     | %c       | %d\n",i,frame,array[i],in_use);

		if(i != 0 && i % 256 == 0){
		   ++frame;
		 }

		 in_use = 0;
	}
}
