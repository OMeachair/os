#include <stdio.h>
#include "limits.h"
#include "input.h"

void take_input(char *physical_mem, char* disk){

	unsigned short user_input, vpn, offset, address, pfn
	char *notInMem;
	unsigned short mask = 0x00FF;

	char data;

	while(user_input != 0xFFFF){

		printf("Enter an address in hexidecimal form, or -1 to quit\n> 0x");
		scanf("%hX", &user_input);

   	vpn = user_input >> 8;
      offset = user_input & mask;
     	printf("VPN - %d\n",vpn);
   	printf("\nOffset - %d\n",offset);

		for(int i = 0;  i < USHRT_MAX; ++i){
			if(physical_mem[i] != 33){
				pfn = i / 256;
				break;
			}
		}

		for(int j = USHRT_MAX; j > 0; --j){
			if(physical_mem[j] != 33){
				notInMem[0] = j / 256;
				notInMem[1] = (j / 256) - 1;
			   break;
			}
		}

		if(vpn == notInMem[0] || vpn == notInMem[1]){
			printf("\nData not in physical memory\n\n");
			//swap(physical_mem, disk, vpn);
		}

		pfn += vpn;
		printf("\nPFN - %d\n",pfn);
		address = pfn << 8;
		address |= offset;
		address += 1;

		printf("\nADDRESS - 0x%04X\n",address);

		data = physical_mem[address];

		if(data == 33){
			printf("\nMemory Location Not In Use\n\n");
		}
		else{
			printf("Data: %c\n",data);
		}
	}


}

void swap(char *physical_mem, char *disk, unsigned short user_input){



	
}

