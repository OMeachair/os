#ifndef MEMORYH
#define MEMORYH

   char *allocate_memory();
   int check_memory(char *,char *);
	void free_memory(char *);
	int generate_random(int, int, int);

#endif

