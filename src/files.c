#include <stdio.h>

FILE *open_file(char *file_name){

	FILE *file = fopen(file_name, "w");

	return file;

}

int check_file(FILE *file, char *file_name){

	if(file == NULL){
   	printf("File '%s' could not be opened\n",file_name);
      return 3;
      }
      
}

void close_file(FILE *file){

	fclose(file);
}

