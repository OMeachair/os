#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <time.h>
#include "src/memory.h"
#include "src/files.h"
#include "src/input.h"
#include "src/data.h"

#define MAXBYTES 20480
#define MINBYTES 2048
#define MININDEX 512

int main(){

	char *physical_mem = allocate_memory();

	check_memory(physical_mem,"physical_mem");

	FILE *physical_mem_txt = open_file("./data/physical_mem.txt");
	FILE *page_table_txt = open_file("./data/page_table.txt");

	check_file(physical_mem_txt, "physical_mem.txt");
	check_file(page_table_txt, "page_table.txt");

	int num_of_bytes = generate_random(MINBYTES, MAXBYTES, 0);

	int index = 1;
	index = generate_random(MININDEX, USHRT_MAX,1);

	printf("%d\n",index / 256);
	physical_mem = bytes_for_process(physical_mem, num_of_bytes,index);

	write_pt_file(physical_mem, page_table_txt, num_of_bytes, index);
	write_pm_file(physical_mem, physical_mem_txt);

	take_input(physical_mem);

	close_file(physical_mem_txt);
	close_file(page_table_txt);
	free_memory(physical_mem);

	return 0;

}
