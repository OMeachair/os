
COMP = gcc
DISTDIR = dist
SRCDIR = src
SRC_C = $(SRCDIR)/*.c
OBJECTS = $(DISTDIR)/*.o
CFLAGS = -c -Wall

link: $(SRC_C) simulate.c
	$(COMP) $? -o $(DISTDIR)/simulate

dist/simulate.o: simulate.c
	$(COMP) $(CFLAGS) $? -o $(DISTDIR)/simulate.o

dist/memory.o: $(SRCDIR)/memory.c
	$(COMP) $(CFLAGS) $? -o $(DISTDIR)/memory.o

dist/files.o: $(SRCDIR)/files.c
	$(COMP) $(CFLAGS) $? -o $(DISTDIR)/files.o

dist/data.o: $(SRCDIR)/data.c
	$(COMP) $(CFLAGS) $? -o $(DISTDIR)/data.o
 
#$(OBJECTS): $(SRC_C) 
#	$(COMP) $(CFLAGS) $? -o $(DISTDIR)/simulate.o
#	$(COMP) $(CFLAGS) $? -o $(DISTDIR)/memory.o
#	$(COMP) $(CFLAGS) $? -o $(DISTDIR)/files.o

clean:
	rm -rf ./$(DISTDIR) && mkdir ./$(DISTDIR)
